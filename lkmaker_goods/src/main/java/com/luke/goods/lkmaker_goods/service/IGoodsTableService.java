package com.luke.goods.lkmaker_goods.service;

import com.github.pagehelper.PageInfo;
import com.luke.goods.lkmaker_goods.model.GoodsTable;

public interface IGoodsTableService {
    PageInfo<GoodsTable> findAll(int cp);
}
