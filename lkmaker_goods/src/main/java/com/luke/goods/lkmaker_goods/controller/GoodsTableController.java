package com.luke.goods.lkmaker_goods.controller;

import com.github.pagehelper.PageInfo;
import com.luke.goods.lkmaker_goods.model.GoodsTable;
import com.luke.goods.lkmaker_goods.service.IGoodsTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Date;

@RestController
@CrossOrigin
public class GoodsTableController {
    @Autowired
    private IGoodsTableService goodsTableService;

    @InitBinder
    public void dateFormat(WebDataBinder binder) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        simpleDateFormat.setLenient(true);
        binder.registerCustomEditor(Date.class,new CustomDateEditor(simpleDateFormat, true));
    }

    @RequestMapping("init")
    public PageInfo<GoodsTable> init(@RequestParam(required = true, defaultValue = "1") int cp){
        return goodsTableService.findAll(cp);
    }
}