package com.luke.goods.lkmaker_goods.dao;

import com.luke.goods.lkmaker_goods.model.GoodsTable;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GoodsTableMapper {
    List<GoodsTable> findAll();

    int insertGoods(GoodsTable goodsTable);

    int deleteGoods(@Param("id")int id);

    int updateGoods(GoodsTable goodsTable);
}
