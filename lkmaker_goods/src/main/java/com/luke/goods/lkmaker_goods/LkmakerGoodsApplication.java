package com.luke.goods.lkmaker_goods;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
//@ImportResource("classpath:spring/applicationContext-*.xml")
@MapperScan("com.luke.goods.lkmaker_goods.dao")
public class LkmakerGoodsApplication {
    public static void main(String[] args) {
        SpringApplication.run(LkmakerGoodsApplication.class, args);
    }

}

