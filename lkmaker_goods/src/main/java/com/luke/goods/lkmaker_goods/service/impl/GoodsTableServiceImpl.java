package com.luke.goods.lkmaker_goods.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.luke.goods.lkmaker_goods.dao.GoodsTableMapper;
import com.luke.goods.lkmaker_goods.model.GoodsTable;
import com.luke.goods.lkmaker_goods.service.IGoodsTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class GoodsTableServiceImpl implements IGoodsTableService {
    @Autowired
    private GoodsTableMapper goodsTableMapper;

    @Override
    public PageInfo<GoodsTable> findAll(int cp) {
        Page<GoodsTable> page = PageHelper.startPage(cp, 2);
        goodsTableMapper.findAll();
        PageInfo<GoodsTable> pageInfo = new PageInfo<>(page);
        return pageInfo;
    }
}
