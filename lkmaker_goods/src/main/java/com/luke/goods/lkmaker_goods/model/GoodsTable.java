package com.luke.goods.lkmaker_goods.model;

import java.io.Serializable;
import java.util.Date;
public class GoodsTable implements Serializable {
    //商品Id
    private int goodsId;
    //商品名字
    private String goodsName;
    //商品金额
    private int goodsMoney;
    //商品描述
    private String goodsDesc;
    //商品图片
    private String goodsImage;
    //商品颜色
    private String goodsColor;
    //商品尺寸
    private String goodsSize;
    private String operationId;
    private Date operationTime;
    private char zfFlag;
    //
    private int merchantsId;
    //标签图
    private String labelImage;
    private String introduceImage;


    public int getGoodsId() {
        return goodsId;
    }
    public void setGoodsId(int goodsId) {
        this.goodsId = goodsId;
    }
    public String getGoodsName() {
        return goodsName;
    }
    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }
    public int getGoodsMoney() {
        return goodsMoney;
    }
    public void setGoodsMoney(int goodsMoney) {
        this.goodsMoney = goodsMoney;
    }
    public String getGoodsDesc() {
        return goodsDesc;
    }
    public void setGoodsDesc(String goodsDesc) {
        this.goodsDesc = goodsDesc;
    }
    public String getGoodsImage() {
        return goodsImage;
    }
    public void setGoodsImage(String goodsImage) {
        this.goodsImage = goodsImage;
    }
    public String getGoodsColor() {
        return goodsColor;
    }
    public void setGoodsColor(String goodsColor) {
        this.goodsColor = goodsColor;
    }
    public String getGoodsSize() {
        return goodsSize;
    }
    public void setGoodsSize(String goodsSize) {
        this.goodsSize = goodsSize;
    }
    public String getOperationId() {
        return operationId;
    }
    public void setOperationId(String operationId) {
        this.operationId = operationId;
    }
    public Date getOperationTime() {
        return operationTime;
    }
    public void setOperationTime(Date operationTime) {
        this.operationTime = operationTime;
    }
    public char getZfFlag() {
        return zfFlag;
    }
    public void setZfFlag(char zfFlag) {
        this.zfFlag = zfFlag;
    }
    public int getMerchantsId() {
        return merchantsId;
    }
    public void setMerchantsId(int merchantsId) {
        this.merchantsId = merchantsId;
    }
    public String getLabelImage() {
        return labelImage;
    }
    public void setLabelImage(String labelImage) {
        this.labelImage = labelImage;
    }
    public String getIntroduceImage() {
        return introduceImage;
    }
    public void setIntroduceImage(String introduceImage) {
        this.introduceImage = introduceImage;
    }
}
