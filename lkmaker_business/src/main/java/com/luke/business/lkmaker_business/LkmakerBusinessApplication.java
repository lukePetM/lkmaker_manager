package com.luke.business.lkmaker_business;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LkmakerBusinessApplication {

    public static void main(String[] args) {
        SpringApplication.run(LkmakerBusinessApplication.class, args);
    }

}

