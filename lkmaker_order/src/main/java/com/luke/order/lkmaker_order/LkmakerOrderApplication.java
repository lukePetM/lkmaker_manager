package com.luke.order.lkmaker_order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LkmakerOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(LkmakerOrderApplication.class, args);
    }

}

